package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    public id.ac.ui.cs.advprog.tutorial1.strategy.FlyBehavior flyBehavior;
    public id.ac.ui.cs.advprog.tutorial1.strategy.QuackBehavior quackBehavior;

    public Duck() {
    }

    public abstract void display();

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void swim() {
        System.out.println("Semua bebek berenang");
    }

    public void setFlyBehavior(id.ac.ui.cs.advprog.tutorial1.strategy.FlyBehavior fb) {
        flyBehavior = fb;
    }

    public void setQuackBehavior(id.ac.ui.cs.advprog.tutorial1.strategy.QuackBehavior qb) {
        quackBehavior = qb;
    }

    // TODO Complete me!
}
