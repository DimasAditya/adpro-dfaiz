package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {
        id.ac.ui.cs.advprog.tutorial1.strategy.Duck mallard = new id.ac.ui.cs.advprog.tutorial1.strategy.MallardDuck();
        mallard.performQuack();
        mallard.performFly();

       
        id.ac.ui.cs.advprog.tutorial1.strategy.Duck model = new id.ac.ui.cs.advprog.tutorial1.strategy.ModelDuck();
        model.performFly();
        model.setFlyBehavior(new id.ac.ui.cs.advprog.tutorial1.strategy.FlyRocketPowered());
        model.performFly();
    }
	

}
