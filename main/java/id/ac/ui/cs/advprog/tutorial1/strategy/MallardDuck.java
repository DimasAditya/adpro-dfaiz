package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends id.ac.ui.cs.advprog.tutorial1.strategy.Duck {
    // TODO Complete me!
    public MallardDuck(){
        quackBehavior = new id.ac.ui.cs.advprog.tutorial1.strategy.Quack();
        flyBehavior = new id.ac.ui.cs.advprog.tutorial1.strategy.FlyWithWings();
    }

    public void display(){
        System.out.println("Mallard");
    }
}
